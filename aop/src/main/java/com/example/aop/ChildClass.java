package com.example.aop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChildClass extends ParentClass {

	@Autowired
	private ParentClass p;

	@Override
	public void method1() {
		p.method2();

	}

}
