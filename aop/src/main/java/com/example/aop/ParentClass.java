package com.example.aop;

public abstract class ParentClass {

	public abstract void method1();
	@Template
	protected void method2(){
		System.out.println("hello world");
	}
}
