package com.example.aop;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AOPHendler {

	@Around("@annotation(com.example.aop.Template)")
	public void executeMethod1(ProceedingJoinPoint proceedingJoinPoint)
			throws Throwable {
		System.out.println("Test");
		Method[] m = proceedingJoinPoint.getThis().getClass().getSuperclass()
				.getSuperclass().getDeclaredMethods();
		for (Method method : m) {
			System.out.println(method.getName());
			if (method.isAnnotationPresent(Template.class)) {
				System.out.println(method.getDeclaredAnnotations()[0]
						.toString());
			}

		}
		proceedingJoinPoint.proceed();

	}
}
