package com.example;

import java.lang.reflect.Method;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.EnableLoadTimeWeaving;

import com.example.aop.ChildClass;
import com.example.aop.ParentClass;
import com.example.aop.Template;

@SpringBootApplication
@EnableAspectJAutoProxy
@ComponentScan
@EnableAutoConfiguration
public class AopApplication {

	
	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(AopApplication.class, args);
		ParentClass p = context.getBean(ChildClass.class);
		p.method1();
		
/*		ChildClass c= new ChildClass();
		Method [] m = c.getClass().getSuperclass().getDeclaredMethods();
		for (Method method : m) {
			System.out.println(method.getName());
			//System.out.println(method.getAnnotations()[0].toString());
			if(method.isAnnotationPresent(Template.class)){
				System.out.println(method.getDeclaredAnnotations()[0].toString());
			}
			
		}*/
		}
	
	
	
	
	
}
